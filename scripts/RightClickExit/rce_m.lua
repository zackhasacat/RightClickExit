local ui = require("openmw.ui")
local core = require("openmw.core")
local menu = require("openmw.menu")

local function onMouseButtonPress(button)
    if menu.getState() ~= menu.STATE.Running then
        return
    end
    if button == 3 and core.isWorldPaused() then
        --   core.sendGlobalEvent("rightClickExit")
        ui._setUiModeStack({})
        end
end


return {
    engineHandlers = {
        onMouseButtonPress = onMouseButtonPress
    }
}
