local ui = require("openmw.ui")
local core = require("openmw.core")
local I = require("openmw.interfaces")

local function rightClickExit()
    if I.UI.getMode() then
        I.UI.setMode()
    end
end
local function onMouseButtonPress(button)
    print(button)
    if button == 3 and core.isWorldPaused() then
--    rightClickExit()
--ui._setUiModeStack({})
end
end


return{
    engineHandlers = {
        onMouseButtonPress = onMouseButtonPress
    },
    eventHandlers = {
        rightClickExit = rightClickExit
    }
}