local world = require("openmw.world")
local core = require("openmw.core")
local I = require("openmw.interfaces")
local types = require("openmw.types")
local anim = require('openmw.animation')


local function rightClickExit()
    world.players[1]:sendEvent("rightClickExit")
end

return{
    eventHandlers = {
        rightClickExit = rightClickExit
    }
}